﻿/*
Author: MAQ Software
Create date: 18/10/2019
Description: Creates Recent upload dynamically
*/
const homeTab = "Home";
var oData;
var info = "";
window.onload = function() {

    $.ajax({
        url: '/getData',
        type: "GET",
        async: false,
        success: function(data) {
            oData = data;
            console.log(oData);
        },
        error: function() {},
        contentType: false,
        processData: false
    });
    // var lookup = {};
    var items = oData;
    var uniqueTags = ["PowerBI", "Cloud", "SharePoint", "Collaboration", "PowerApp", "UI/UX", "CodeSnippets", "Lorem", "LogicApps", "AI", "Data", "PowerAutomate"];
    // for (var item, i = 0; item = items[i++];) {
    //     for(var j=0;j< item.Tags.length;j++) {
    //         var name = item.Tags[j];
    //         if (!(name in lookup)) {
    //             lookup[name] = 1;
    //             uniqueTags.push(name);
    //         }
    //     }
    // }
    createUniqueTags(uniqueTags, uniqueTags.length, ".widget-tag");
    var oDataRecentLearning = oData.filter(u => u.Title == 'Learnings').sort(function(value1, value2) { return value1.UTCDateTime < value2.UTCDateTime ? 1 : -1; }).slice(0, 3);
    createDivRecentLearning(oDataRecentLearning, Object.keys(oDataRecentLearning).length, "#learnings_recent")
    var oDataRecent = oData.sort(function(value1, value2) { return value1.UTCDateTime < value2.UTCDateTime ? 1 : -1; }).slice(0, 8);
    var oDataPopular = oData.sort(function(value1, value2) { return value1.DownloadCount < value2.DownloadCount ? 1 : -1; }).slice(0, 8);
    createElement(oDataRecent, Object.keys(oDataRecent).length, "#recentUploads");
    createElement(oDataPopular, Object.keys(oDataPopular).length, "#popularUploads");
    $('.category-tabs').delegate('li', 'click', function() {
        $(".nav-item").removeClass("active");
        $(this).addClass("active");
        if ($(this).text() == homeTab) {
            ApplyTagFilter(oData);
        } else {
            ApplyTagFilter(oData);
            $(".Sortbuttons-text").removeClass("selected-sort");
            $('#sortbutton').hide();
        }
    });
    $("#sortuploads").click(function() {
        if ($('div.RecUploads').text() == 'Sort by >') {
            $('div.RecUploads').text('Sort by ^').css('cursor', 'pointer');
            $('#sortbutton').show();
        } else if ($('div.RecUploads').text() == 'Sort by ^') {
            $('div.RecUploads').text('Sort by >').css('cursor', 'pointer');
            $('#sortbutton').hide();
        }
    });
    $("#DnewToold").click(function() {
        var dataset = JSON.parse(JSON.stringify(oData));
        if ($(this).hasClass("selected-sort")) {
            $(".Sortbuttons-text").removeClass("selected-sort");
            ApplyTagFilter(dataset);
        } else {
            $(".Sortbuttons-text").removeClass("selected-sort");
            $(this).addClass("selected-sort");
            var exposeData = dataset.sort(function(value1, value2) { return value1.UTCDateTime < value2.UTCDateTime ? 1 : -1; });
            ApplyTagFilter(exposeData);
        }
    });
    $("#DoldTonew").click(function() {
        var dataset = JSON.parse(JSON.stringify(oData));
        if ($(this).hasClass("selected-sort")) {
            $(".Sortbuttons-text").removeClass("selected-sort");
            ApplyTagFilter(dataset);
        } else {
            $(".Sortbuttons-text").removeClass("selected-sort");
            $(this).addClass("selected-sort");
            var exposeData = dataset.sort(function(value1, value2) { return value1.UTCDateTime > value2.UTCDateTime ? 1 : -1; });
            ApplyTagFilter(exposeData);
        }
    });
    $("#VhighTolow").click(function() {
        var dataset = JSON.parse(JSON.stringify(oData));
        if ($(this).hasClass("selected-sort")) {
            $(".Sortbuttons-text").removeClass("selected-sort");
            ApplyTagFilter(dataset);
        } else {
            $(".Sortbuttons-text").removeClass("selected-sort");
            $(this).addClass("selected-sort");
            var exposeData = dataset.sort(function(value1, value2) { return value1.ViewCount < value2.ViewCount ? 1 : -1; });
            ApplyTagFilter(exposeData);
        }
    });
    $("#VlowTohigh").click(function() {
        var dataset = JSON.parse(JSON.stringify(oData));
        if ($(this).hasClass("selected-sort")) {
            $(".Sortbuttons-text").removeClass("selected-sort");
            ApplyTagFilter(dataset);
        } else {
            $(".Sortbuttons-text").removeClass("selected-sort");
            $(this).addClass("selected-sort");
            var exposeData = dataset.sort(function(value1, value2) { return value1.ViewCount > value2.ViewCount ? 1 : -1; });
            ApplyTagFilter(exposeData);
        }
    });
    $("#DhighTolow").click(function() {
        var dataset = JSON.parse(JSON.stringify(oData));
        if ($(this).hasClass("selected-sort")) {
            $(".Sortbuttons-text").removeClass("selected-sort");
            ApplyTagFilter(dataset);
        } else {
            $(".Sortbuttons-text").removeClass("selected-sort");
            $(this).addClass("selected-sort");
            var exposeData = dataset.sort(function(value1, value2) { return value1.DownloadCount < value2.DownloadCount ? 1 : -1; });
            ApplyTagFilter(exposeData);
        }
    });
    $("#DlowTohigh").click(function() {
        var dataset = JSON.parse(JSON.stringify(oData));
        if ($(this).hasClass("selected-sort")) {
            $(".Sortbuttons-text").removeClass("selected-sort");
            ApplyTagFilter(dataset);
        } else {
            $(".Sortbuttons-text").removeClass("selected-sort");
            $(this).addClass("selected-sort");
            var exposeData = dataset.sort(function(value1, value2) { return value1.DownloadCount > value2.DownloadCount ? 1 : -1; });
            ApplyTagFilter(exposeData);
        }
    });
    $('#tags-tab').delegate('li a', 'click', function() {
        var dataset = JSON.parse(JSON.stringify(oData));
        if ($(this).hasClass("selected-sort")) {
            $(this).removeClass("selected-sort");
        } else {
            $(this).addClass("selected-sort");
        }
        checkSortingOption(dataset);
    });
    $("#searchCategory").keyup(function() {
        var dataset = JSON.parse(JSON.stringify(oData));
        var searchTerm = $("#searchCategory").val();
        var convertLowerSerachTerm = searchTerm.toLowerCase();
        if (!searchTerm) {
            checkSortingOption(dataset);
        } else {
            var results = dataset.filter(function(dataTag) {
                return dataTag.Name.toLowerCase().indexOf(convertLowerSerachTerm) > -1;
            });
            checkSortingOption(results);
        }

    });
    $(".selected-tool").click(function() {
        info = oData.filter(i => i.ID === parseInt(document.getElementById("CardID").innerHTML, 10))[0];
        $.ajax({
            url: '/toolDetails',
            type: "POST",
            async: false,
            data: info,
            success: function(data) {

                console.log(parseInt(document.getElementById("CardID").innerHTML, 10));
                console.log(info);

            },
            error: function() {},
            contentType: false,
            processData: false
        });
    });
}

function ApplyTagFilter(oData) {
    var selectedTags = [];
    $("ul#tags-tab li").each(function() {
        if ($(this).children().hasClass("selected-sort")) {
            var requiredTag = $(this);
            selectedTags.push(requiredTag[0].innerText);
        }
    });
    var cdata = oData;
    var tagDataset = [];
    if (selectedTags.length != 0) {
        for (var p = 0; p < selectedTags.length; p++) {
            var createddataset = cdata.filter(m => jQuery.inArray(selectedTags[p], Object.values(JSON.parse(m.Tags))) !== -1);
            var ab = 0;
            while (ab < createddataset.length) {
                var item = createddataset[ab];
                tagDataset.push(item);
                ab++;
            }
        }
        var uniqueTags = tagDataset.filter((arr, index, self) => index === self.findIndex((t) => (t.id === arr.id)));
        checkTabAndPopulateData(uniqueTags);
    } else {
        checkTabAndPopulateData(cdata);
    }
}

function checkTabAndPopulateData(uniqueTags) {
    var active_tabs = $("#tabs li.active");
    var activeTab = active_tabs[0].innerText;
    if (activeTab === homeTab) {
        createHomeTab(uniqueTags);
    } else {
        sortCards(uniqueTags);
        $('div.RecUploads').text('Sort by >').css('cursor', 'pointer');
        $('#sortbutton').hide();
    }
}

function sortCards(DataForCards) {
    var active_tabs = $("#tabs li.active");
    var activeTab = active_tabs[0].innerText;
    var applySort = DataForCards;
    PerformFilter(applySort, activeTab);
}

function PerformFilter(DataName, SelectType) {
    $('#nonEmptyCard').hide();
    var oDataTools = DataName.filter(entry => entry.Title === SelectType);
    $('#recentUploads').children().not(':first-child').not(":nth-child(2)").remove();
    $("#popularUploads").empty();
    createElement(oDataTools, Object.keys(oDataTools).length, "#recentUploads");
    $(".PopUploads").hide();
    if (oDataTools.length == 0) {
        $('#nonEmptyCard').show();
    }

}

function createElement(data, clength, divisionUsed) {
    var _newDivPopularElement = '';
    for (var i = 0; i < clength; i++) {
        _newDivPopularElement += "<div class='cardborder selected-tool' id = 'card" + i + "'> <div class='card-body'> <div class='card-title'>" + data[i]["Title"] + "</div>" +
            "<div class='card-name'>" + data[i]["Name"] + "</div>" +
            "<div class='card-text'>" + data[i]["Text"] + "</div>" +
            "<div class='row flex-xl-nowrap infoContainer'>" +
            "<div class='col-md-8 ownerContainer'>" +
            "<img src='/vendor/images/personicon.svg'>" +
            "<span class='ownerName'>" + data[i]["OwnerName"] + "</span>" +
            "</div>" +
            "<div class='col-sm-2 downloadContainer'>" +
            "<span class='downloadicon'><img src='/vendor/images/downloadicon.svg'></span>" +
            "<span class='count'>" + data[i]["DownloadCount"] + "</span>" +
            "</div>" +
            "<div id = \"CardID\" style= \"display:none\">" + data[i]["ID"] + "</div>" +
            "<div class='col-sm-2 viewContainer'>" +
            "<span class='viewicon'><img src='/vendor/images/viewicon.svg'></span>" +
            "<span class='count'>" + data[i]["ViewCount"] + "</span></div></div></div></div>";
    }
    $(divisionUsed).append(_newDivPopularElement);
}

function createHomeTab(oData) {
    $('#sortbutton').hide();
    $('#nonEmptyCard').hide();
    var oDataRecent = oData.sort(function(value1, value2) { return value1.UTCDateTime < value2.UTCDateTime ? 1 : -1; }).slice(0, 8);
    var oDataPopular = oData.sort(function(value1, value2) { return value1.DownloadCount < value2.DownloadCount ? 1 : -1; }).slice(0, 8);
    $('#recentUploads').children().not(':first-child').not(":nth-child(2)").remove();
    $("#popularUploads").empty();
    $('div.RecUploads').text('Recent Uploads').css('cursor', 'text');
    $(".PopUploads").show();
    createElement(oDataRecent, Object.keys(oDataRecent).length, "#recentUploads");
    createElement(oDataPopular, Object.keys(oDataPopular).length, "#popularUploads");
}

function createUniqueTags(data, clength, divisionUsed) {
    var _newDivUniqueTag = '';
    for (var i = 0; i < clength; i++) {
        _newDivUniqueTag += ("<li><a class='blogcategories' data-filter='." + data[i] + " '>" + data[i] + "</a></li>");
    }
    $(divisionUsed).append(_newDivUniqueTag);
}

function createDivRecentLearning(data, clength, divisionUsed) {
    var _newRecentLearning = '';
    for (var i = 0; i < clength; i++) {
        _newRecentLearning += ("<div class='learningsContainer'> <div class='card-body'> <div class='card-title'>" + data[i].Name + "</div>" +
            "<div class='card-text'>" + data[i].Text + "</div>" +
            "<a href='#' class='readmorelink'>Go somewhere </a>" +
            "</div> </div>");
    }
    $(divisionUsed).append(_newRecentLearning);
}

function checkSortingOption(data) {
    var dataset = data;
    if ($(".Sortbuttons-text").hasClass("selected-sort")) {
        var obj = document.getElementsByClassName("Sortbuttons-text selected-sort");
        var id = obj[0].id;
        if (id = DnewToold) {
            dataset = dataset.sort(function(value1, value2) { return value1.UTCDateTime < value2.UTCDateTime ? 1 : -1; });
        } else if (id = DoldTonew) {
            dataset = dataset.sort(function(value1, value2) { return value1.UTCDateTime > value2.UTCDateTime ? 1 : -1; });
        } else if (id = VhighTolow) {
            dataset = dataset.sort(function(value1, value2) { return value1.ViewCount < value2.ViewCount ? 1 : -1; });
        } else if (id = VlowTohigh) {
            dataset = dataset.sort(function(value1, value2) { return value1.ViewCount > value2.ViewCount ? 1 : -1; });
        } else if (id = DhighTolow) {
            dataset = dataset.sort(function(value1, value2) { return value1.DownloadCount < value2.DownloadCount ? 1 : -1; });
        } else if (id = DlowTohigh) {
            dataset = dataset.sort(function(value1, value2) { return value1.DownloadCount > value2.DownloadCount ? 1 : -1; });
        }
    } else {
        console.log("not found");
    }
    ApplyTagFilter(dataset);
}

function submitTechHubProject() {
    var selTags = [];
    $.each($(".tagSelect option:checked"), function() {
        selTags.push($(this).val());
    });
    var tagsNeeded = {};
    for (var i = 1; i <= selTags.length; ++i) {
        tagsNeeded["key" + i] = selTags[i];
    }
    var sendProjectData = {
        Title: $("select.category").children("option:selected").val(),
        Name: $("#name").val(),
        Text: $("#description").val(),
        OwnerName: $("#author").val(),
        DownloadCount: 100,
        ViewCount: 11,
        Tags: JSON.stringify(tagsNeeded),
        ProjectURL: $("#projecturl").val(),
        ProjectID: $("#projectid").val(),
        AccessToken: $("#accesstoken").val(),
        PointOfContact: $("#poc").val(),
        Version: $("#version").val()
    };
    let fObj = new FormData();
    fObj.append("data", JSON.stringify(sendProjectData));
    $.ajax({
        url: '/submitProject',
        type: "POST",
        success: function(data) {
            alert("success");
            window.location.reload();
        },
        error: function() {},
        contentType: false,
        processData: false,
        data: fObj
    });
}
var express = require('express');
var cors = require('cors');
var app = express();
var router = express.Router();
var bodyParser = require('body-parser');
var cons = require('consolidate');
var path = require('path');
app.engine('html', cons.swig)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
var session =require('express-session');
app.use(cors({credentials: true, origin: true}));
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(session({
  secret: 'secretkey',
  resave: true,
  saveUninitialized: false
}));  

require('./controllers/application.router')(app, router);

var server = app.listen(process.env.PORT||9000 , () => {     
  var host = server.address().address;
  var port = server.address().port;
});
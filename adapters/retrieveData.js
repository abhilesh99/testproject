var sql = require('mssql'),
    config = require("../config"),
    constants = require('../constants');
var TYPES = require('tedious').TYPES;
var Connection = require('tedious').Connection;
var Request = require('tedious').Request;

module.exports = {
    executeQuery: function(opcode, oData) {
        let respRows = [];
        return new Promise((resolve, reject) => {
            var table = {
                columns: [
                    { name: 'ParamKey', type: TYPES.VarChar },
                    { name: 'ParamValue', type: TYPES.VarChar }
                ],
                rows: [
                    ['opcode', opcode]
                ]
            };
            for (var params in oData) {
                var addRows = [];
                addRows.push(params);
                addRows.push(oData[params]);
                table.rows.push(addRows);
            }
            console.log(table);

            // var connection = new Connection(config.db);
            // connection.on('connect', function(err) {
            //     // If no error, then good to proceed.  
            //     console.log("Connected");
            //     executeStatement();
            // });

            var connection = new Connection(config.db);
            connection.on('connect', err => {
                if (err) {
                    console.error(err);
                } else {
                    executeStatement();
                }
            });

            function executeStatement() {
                var request = new Request("process.uspExecuteQuery", function(err) {
                    if (err) {
                        console.log(err);
                    }
                });
                request.addParameter('parameters', TYPES.TVP, table);
                request.on('error', function(er) {
                    context.log.error(er);
                    connection.close();
                    // return res({ error: { code: 3, msg: er } });
                });
                var inputAttr = {};

                request.on('row', function(columns) {
                    columns.forEach(function(column) {
                        inputAttr[column.metadata.colName] = column.value;
                    });
                    respRows.push(inputAttr);
                    inputAttr = {};
                });

                connection.callProcedure(request);
                try {
                    request.on('requestCompleted', function() {
                        connection.close();
                        resolve(respRows);
                    });
                } catch (err) {
                    reject(err);
                }
            }
        })
    }
}
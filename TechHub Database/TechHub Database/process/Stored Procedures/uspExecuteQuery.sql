﻿


/* =================================================
Project Name: TechHub
Module Name: Test
Purpose: Entry point for any database call.

-------------------------------------------------------------------------------------------------------------------------------------*/
       /*====================================================================================================================================
       BUSINESS RULES
 ======================================================================================================================================
              1. Inputs
                     a. @Parameters Type
              2. Outputs
                     a. Datatable
              3. Internal Actions
                     a. Searches for the specified opcode and executes the query passing the parameters
 
-------------------------------------------------------------------------------------------------------------------------------------*/
       /*====================================================================================================================================

 
Parameter Info
   @Parameters will contain the opcode and other parameters the specific query might need
*/

CREATE PROCEDURE [process].[uspExecuteQuery] (@parameters dbo.Parameters READONLY)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets
	SET NOCOUNT ON;

	DECLARE @opCode INT

	SELECT @opCode = ParamValue
	FROM @Parameters
	WHERE ParamKey = 'opcode'

	--0 to 99 Test
	IF (@opCode BETWEEN 0 AND 99)
	BEGIN
	IF (@opCode = 0)
	BEGIN
		SELECT * FROM [dbo].[TechData]
	END
	ELSE IF (@opCode = 1)
	BEGIN
		EXEC [project].[uspInsertProjectInfo] @parameters
	END
	END

END
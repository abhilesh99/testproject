﻿CREATE TYPE [dbo].[Parameters] AS TABLE (
    [ParamKey]   VARCHAR (20)   NULL,
    [ParamValue] NVARCHAR (MAX) NULL);


﻿CREATE TABLE [dbo].[TechData] (
    [ProjectID]     INT           IDENTITY (1, 1) NOT NULL,
    [Title]         VARCHAR (30)  NULL,
    [Name]          VARCHAR (100) NULL,
    [Text]          VARCHAR (255) NULL,
    [OwnerName]     VARCHAR (30)  NULL,
    [DownloadCount] VARCHAR (5)   NULL,
    [ViewCount]     VARCHAR (5)   NULL,
    [Tags]          VARCHAR (200) NULL,
    [UTCDateTime]   DATETIME2 (7) NULL
);


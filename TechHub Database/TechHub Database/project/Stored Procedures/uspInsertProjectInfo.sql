﻿
/* =================================================
Project Name: TechHub
Module Name: Login Page
Purpose: Inserts the project Details

-------------------------------------------------------------------------------------------------------------------------------------*/
       /*====================================================================================================================================
       BUSINESS RULES
 ======================================================================================================================================
              1. Inputs
                     a. @Parameters Type
              2. Outputs
                     a. Datatable
              3. Internal Actions
                     a. Searches for the specified opcode and executes the query passing the parameters
 
-------------------------------------------------------------------------------------------------------------------------------------*/
       /*====================================================================================================================================

 
Parameter Info
   @Parameters will contain the opcode and other parameters the specific query might need
 
Return Info:
 
Test Scripts:
 
Revision History:
 ================================================
 Change History
 ========================================================================================================================== 
 Date				Updated BY					Description
 2019/02/12			Darshan Darak				Added Stored Procedure
 ==========================================================================================================================
*/

CREATE   PROCEDURE [project].[uspInsertProjectInfo] (@parameters dbo.Parameters READONLY)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE  @Title varchar(30), @Name varchar(100), @Text varchar(255), @OwnerName varchar(30), @DownloadCount varchar(5), @ViewCount varchar (5), @Tags varchar (200), @UTCDateTime [datetime2](7), @errorCode INT = 1, @ROWCOUNT INT;

		SELECT @Title = ParamValue
        FROM @parameters
        WHERE ParamKey = 'Title';

		SELECT @Name = ParamValue
        FROM @parameters
        WHERE ParamKey = 'Name';

		SELECT @Text = ParamValue
        FROM @parameters
        WHERE ParamKey = 'Text';

		SELECT @OwnerName = ParamValue
        FROM @parameters
        WHERE ParamKey = 'OwnerName';

		SELECT @DownloadCount = ParamValue
        FROM @parameters
        WHERE ParamKey = 'DownloadCount';

		SELECT @ViewCount = ParamValue
        FROM @parameters
        WHERE ParamKey = 'ViewCount';

		SELECT @Tags = ParamValue
        FROM @parameters
        WHERE ParamKey = 'Tags';

		SELECT @UTCDateTime = ParamValue
        FROM @parameters
        WHERE ParamKey = 'UTCDateTime';

 
	   INSERT INTO [dbo].[TechData]
	   (
	   [Title],[Name], [Text], [OwnerName], [DownloadCount], [ViewCount], [Tags], [UTCDateTime]
	   )
	   VALUES
	   (
	    @Title, @Name, @Text, @OwnerName, @DownloadCount, @ViewCount, @Tags, @UTCDateTime
	   )
	SELECT 0 AS 'ErrorCode';
END
module.exports = (app, router) => {
    const projectsController = require('../model/techHubMethods');
    router.use((req, res, next) => {
        next();
    });
    app.get('/Home', function(req, res) {
        res.render('index.html');
    });
    app.post('/toolDetails', function(req, res) {
        //var info1 = req.body;
        res.render('toolDetails.html');
    });
    app.post("/submitProject", projectsController.insertForm);
    app.get('/getData', projectsController.FetchProjectData);
}
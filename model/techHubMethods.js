var retrieveData = require('../adapters/retrieveData');
var multiparty = require('multiparty');

exports.insertForm = async function(req, res) {
    var form = new multiparty.Form();
    var response = [];
    form.on('part', async function(part) {
        part.resume();
    });
    form.on('field', async function(field, value) {
        response[field] = value
    });
    form.on('error', async function(err) {
        console.log(err);
    });
    form.on('close', async function() {
        var projectData = JSON.parse(response.data);
        console.log(projectData);
        var insertData = ((await retrieveData.executeQuery(1, {
            "Title": projectData.Title,
            "Name": projectData.Name,
            "Text": projectData.Text,
            "OwnerName": projectData.OwnerName,
            "DownloadCount": projectData.DownloadCount,
            "ViewCount": projectData.ViewCount,
            "Tags": projectData.Tags,
            "ProjectURL": projectData.ProjectURL,
            "ProjectID": projectData.ProjectID,
            "AccessToken": projectData.AccessToken,
            "PointOfContact": projectData.PointOfContact,
            "Version": projectData.Version
        })));
        if (insertData[0].ErrorCode == 0) {
            res.redirect("/Home");
        }
    });
    form.parse(req);
}
exports.FetchProjectData = async function(req, res) {
    var fetchData = ((await retrieveData.executeQuery(0, {})));

    res.send(fetchData);
}